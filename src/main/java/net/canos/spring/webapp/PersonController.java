package net.canos.spring.webapp;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/person")
public class PersonController {
	Logger log = Logger.getLogger(getClass());

	@Autowired
	private PersonService personService;

	/**
	 * @param personId si <= 0 devuelve null, si no objeto vacío
	 * @return
	 */
	@RequestMapping(value="/{personId}",method= RequestMethod.GET)
	public ResponseEntity<?> get(@PathVariable("personId") Integer personId){
		log.info("GET");
		PersonDTO person = personService.findById(personId);
		
		if(person == null) {
			throw new ResourceNotFoundException("Person not found");
		}
		return new ResponseEntity<PersonDTO>(person,HttpStatus.OK);
	}
	
	@RequestMapping(value="/save",method= RequestMethod.POST)
	public ResponseEntity<?> create(Person person){
		log.info("POST");
		PersonDTO person_new = personService.create(person);
		
		if(person == null) {
			throw new ResourceNotFoundException("Person not found");
		}
		return new ResponseEntity<PersonDTO>(person_new,HttpStatus.OK);
	}
	
	@RequestMapping(value="/{personId}/update",method= RequestMethod.PUT)
	public ResponseEntity<?> save(Person person){
		log.info("PUT");
		PersonDTO person_new = personService.save(person);

		if(person == null) {
			throw new ResourceNotFoundException("Person not found");
		}
		return new ResponseEntity<PersonDTO>(person_new,HttpStatus.OK);
	}
	
	@RequestMapping(value="/{personId}/delete",	method	= RequestMethod.DELETE)
	public ResponseEntity<?> delete(@PathVariable("personId") Integer personId){
		log.info("DELETE");
		personService.delete(personId);
		
		return new ResponseEntity<PersonDTO>(HttpStatus.OK);
	}
	
}
